# Generated by Django 2.2.9 on 2020-01-03 08:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0005_auto_20200102_2306'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('no', models.AutoField(primary_key=True, serialize=False, verbose_name='编号')),
                ('username', models.CharField(max_length=20, unique=True, verbose_name='用户名')),
                ('password', models.CharField(max_length=32)),
                ('email', models.CharField(default='', max_length=256, verbose_name='邮箱')),
                ('tel', models.CharField(max_length=11, verbose_name='手机号')),
                ('reg_date', models.DateTimeField(auto_now_add=True, verbose_name='注册日期')),
                ('last_visit', models.DateTimeField(verbose_name='最后访问')),
            ],
            options={
                'verbose_name': '用户',
                'verbose_name_plural': '用户',
                'db_table': 'tb_user',
            },
        ),
        migrations.AlterField(
            model_name='teacher',
            name='photo',
            field=models.ImageField(default='', max_length=512, upload_to='images', verbose_name='头像'),
        ),
    ]
