import hashlib


def to_md5_hex(content):
    """为字符串生成MD5摘要"""
    return hashlib.md5(content.encode()).hexdigest()
